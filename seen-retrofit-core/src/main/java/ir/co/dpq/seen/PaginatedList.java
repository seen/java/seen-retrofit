package ir.co.dpq.seen;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Test class
 * 
 * @author maso
 *
 */
public class PaginatedList<T> {

	/**
	 * اندیس صفحه جاری را تعیین می‌کند
	 */
	@JsonProperty("current_page")
	int currentPage;

	/**
	 * تعداد گزینه‌های در صفحه را تعیین می‌کند
	 */
	@JsonProperty("items_per_page")
	int itemsPerPage;

	/**
	 * تعداد کل صفحه‌ها را تعیینن می‌کند.
	 */
	@JsonProperty("page_number")
	int pageNumber;

	/**
	 * List of items
	 */
	@JsonProperty("items")
	List<T> items;

	public int getCounts() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}

	/**
	 * @return the currentPage
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * @param currentPage
	 *            the currentPage to set
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @return the itemsPerPage
	 */
	public int getItemsPerPage() {
		return itemsPerPage;
	}

	/**
	 * @param itemsPerPage
	 *            the itemsPerPage to set
	 */
	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	/**
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber
	 *            the pageNumber to set
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * @return the items
	 */
	public List<T> getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(List<T> items) {
		this.items = items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return Util.toString(this);
	}
}