package ir.co.dpq.seen;

import java.util.Map;

// XXX: maso, 2017 Adding data anotation
public class QueryParameter {

	private String query;

	private int page;
	private int itemPerPage;

	private String sortKey;
	private SortOrder sortOrder;

	private String filterKey;
	private String filterValue;

	public String getQuery() {
		return query;
	}

	public QueryParameter setQuery(String query) {
		this.query = query;
		return this;
	}

	public int getPage() {
		return page;
	}

	public QueryParameter setPage(int page) {
		this.page = page;
		return this;
	}

	public String getSortKey() {
		return sortKey;
	}

	public QueryParameter setSortKey(String sortKey) {
		this.sortKey = sortKey;
		return this;
	}

	public SortOrder getSortOrder() {
		if (sortOrder == null)
			return SortOrder.desc;
		return sortOrder;
	}

	public QueryParameter setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}

	public String getFilterKey() {
		return filterKey;
	}

	public QueryParameter setFilterKey(String filterKey) {
		this.filterKey = filterKey;
		return this;
	}

	public String getFilterValue() {
		return filterValue;
	}

	public QueryParameter setFilterValue(String filterValue) {
		this.filterValue = filterValue;
		return this;
	}

	/**
	 * فیلتر مورد نیاز را تعیین می‌کند.
	 * 
	 * @param key
	 * @param value
	 */
	public QueryParameter setFilter(String key, String value) {
		setFilterKey(key);
		setFilterValue(value);
		return this;
	}

	public int getItemPerPage() {
		return itemPerPage;
	}

	public QueryParameter setItemPerPage(int itemPerPage) {
		this.itemPerPage = itemPerPage;
		return this;
	}

	/**
	 * Convert to map
	 * 
	 * @see Util#convertValue(Object)
	 * @return
	 */
	public Map<String, Object> requestMap() {
		return Util.convertValue(this);
	}
}
