package ir.co.dpq.seen;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Basics of models
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
public class Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Object unique id
	 * 
	 * @note ID sends from server allways and you ar not allowd to send it back to
	 *       the server.
	 */
	@JsonProperty(value = "id", access = Access.WRITE_ONLY)
	Long id;

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Model setId(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Convert to map
	 * 
	 * @see Util#convertValue(Object)
	 * @return
	 */
	public Map<String, Object> requestMap() {
		return Util.convertValue(this);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return Util.toString(this);
	}
}
