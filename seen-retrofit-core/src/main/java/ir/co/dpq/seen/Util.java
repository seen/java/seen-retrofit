package ir.co.dpq.seen;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author maso
 *
 */
public class Util {
	public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	
	static {
		OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
		OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static Map<String, Object> convertValue(Object obj){
		@SuppressWarnings("unchecked")
		Map<String, Object> map = OBJECT_MAPPER.convertValue(obj, Map.class);
		return map;
	}
	
	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static String toString(Object obj) {
		try {
			return OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO: maso, 2017: convert to Seen Exception
			throw new RuntimeException(e);
		}
	}
	
}
