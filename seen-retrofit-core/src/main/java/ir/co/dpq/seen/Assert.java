package ir.co.dpq.seen;

public class Assert {

	public static void assertNotNull(Object object, String message) {
		if(object == null){
			throw new RuntimeException(message);
		}
	}

}
