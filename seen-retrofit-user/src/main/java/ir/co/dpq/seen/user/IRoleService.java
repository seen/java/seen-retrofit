package ir.co.dpq.seen.user;

import java.util.Map;

import ir.co.dpq.seen.PaginatedList;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface IRoleService {

	@GET("role/find")
	Call<PaginatedList<Role>> list(@QueryMap Map<String, Object> queryParams);

	@FormUrlEncoded
	@POST("role/new")
	Call<Role> create(@FieldMap Map<String, Object> params);

	@GET("role/{roleId}")
	Call<Role> get(@Path("roleId") Long id);

	@FormUrlEncoded
	@POST("role/{roleId}")
	Call<Role> update(@Path("roleId") Long id, @FieldMap Map<String, Object> params);

	@DELETE("role/{roleId}")
	Call<Role> delete(@Path("roleId") Long id);

	@GET("role/{roleId}/user/find")
	Call<PaginatedList<User>> users(@Path("roleId") Long id, @QueryMap Map<String, Object> queryParams);

	@POST("role/{roleId}/user/new")
	Call<Role> addRole(@Path("roleId") Long id, @Field("user_id") Long userId);

	@DELETE("role/{roleId}/user/{userId}")
	Call<Role> deleteRole(@Path("roleId") Long id, @Path("userId") Long userId);

	@GET("role/{roleId}/group/find")
	Call<PaginatedList<Group>> list(@Path("roleId") Long id, @QueryMap Map<String, Object> queryParams);

	@POST("role/{roleId}/group/new")
	Call<Role> addGroup(@Path("roleId") Long id, @Field("group_id") Long groupId);

	@DELETE("role/{roleId}/group/{groupId}")
	Call<Role> deleteGroup(@Path("roleId") Long id, @Path("group_id") Long groupId);
}
