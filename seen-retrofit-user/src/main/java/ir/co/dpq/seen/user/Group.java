package ir.co.dpq.seen.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import ir.co.dpq.seen.Model;

/**
 * Group data mdoel
 * 
 * @author maso <mostafa.barmshory@dpq.co.ir>
 *
 */
public class Group extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "version", access = Access.WRITE_ONLY, defaultValue = "0", index = 1)
	@JsonPropertyDescription("Group version")
	private Long version;

	@JsonProperty(value = "name", access = Access.READ_WRITE, defaultValue = "name", index = 0)
	@JsonPropertyDescription("Group name")
	private String name;

	@JsonProperty(value = "description", access = Access.READ_WRITE, defaultValue = "description", index = 0)
	@JsonPropertyDescription("Group description")
	private String description;

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
