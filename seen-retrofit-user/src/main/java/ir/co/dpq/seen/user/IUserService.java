package ir.co.dpq.seen.user;

import java.util.Map;

import ir.co.dpq.seen.PaginatedList;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Access layer of users
 * 
 * @author maso <mostafa.barmshory@dpq.co.ir>
 * @author hadi <mohammad.hadi.mansouri@dpq.co.ir>
 */
public interface IUserService {

	/**
	 * User login
	 * 
	 * @note Remote server MAY use cookies to manage state of a user. Be sure the
	 *       backend http client keeps cookies.
	 * 
	 * @param username
	 * @param password
	 * @param callback
	 */
	@FormUrlEncoded
	@POST("user/login")
	Call<User> login(@Field("login") String username, @Field("password") String password);

	/**
	 * User logout
	 * 
	 * @param callback
	 */
	@GET("user/logout")
	Call<User> logout();

	/**
	 * Current user
	 * 
	 * @param callback
	 */
	@GET("user")
	Call<User> getCurrentUser();

	@GET("user/find")
	Call<PaginatedList<User>> list(@QueryMap Map<String, Object> queryParams);

	@FormUrlEncoded
	@POST("user/new")
	Call<User> create(@FieldMap Map<String, Object> params);

	@GET("user/{userId}")
	Call<User> get(@Path("userId") long userId);

	@FormUrlEncoded
	@POST("user/{userId}")
	Call<User> update(@Path("userId") Long id, @FieldMap Map<String, Object> params);

	@DELETE("user/{userId}")
	Call<User> delete(@Path("userId") Long id);

	@GET("user/{userId}/role/find")
	Call<PaginatedList<Role>> roles(@Path("userId") Long id, @QueryMap Map<String, Object> queryParams);

	@POST("user/{userId}/role/new")
	Call<User> addRole(@Path("userId") Long id, @Field("role_id") Long roleId);

	@DELETE("user/{userId}/role/{roleId}")
	Call<User> deleteRole(@Path("userId") Long id, @Path("role_id") Long roleId);

	@GET("user/{userId}/group/find")
	Call<PaginatedList<Group>> groups(@Path("userId") Long id, @QueryMap Map<String, Object> queryParams);

	@POST("user/{userId}/group/new")
	Call<User> addGroup(@Path("userId") Long id, @Field("group_id") Long groupId);

	@DELETE("user/{userId}/group/{groupId}")
	Call<User> deleteGroup(@Path("userId") Long id, @Path("group_id") Long groupId);
}
