package ir.co.dpq.seen.user;

import java.util.Map;

import ir.co.dpq.seen.PaginatedList;
import retrofit2.Callback;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface IGroupService {

	@GET("group/find")
	Callback<PaginatedList<Group>> list(@QueryMap Map<String, Object> queryParams);

	@FormUrlEncoded
	@POST("group/new")
	Callback<Group> create(@FieldMap Map<String, Object> params);

	@GET("group/{groupId}")
	Callback<Group> get(@Path("groupId") Long id);

	@FormUrlEncoded
	@POST("group/{groupId}")
	Callback<Group> update(@Path("groupId") Long id, @FieldMap Map<String, Object> params);

	@DELETE("group/{groupId}")
	Callback<Group> delete(@Path("groupId") Long id);

	@GET("group/{groupId}/role/find")
	Callback<PaginatedList<Group>> roles(@Path("groupId") Long id, @QueryMap Map<String, Object> queryParams);

	@POST("group/{groupId}/role/new")
	Callback<Group> addRole(@Path("groupId") Long id, @Field("role_id") Long roleId);

	@DELETE("group/{groupId}/role/{roleId}")
	Callback<Group> deleteRole(@Path("groupId") Long id, @Path("role_id") Long roleId);

	@GET("group/{groupId}/user/find")
	Callback<PaginatedList<User>> users(@Path("groupId") Long id, @QueryMap Map<String, Object> queryParams);

	@POST("group/{groupId}/user/new")
	Callback<Group> addUser(@Path("userId") Long id, @Field("user_id") Long userId);

	@DELETE("group/{groupId}/user/{userId}")
	Callback<Group> deleteUser(@Path("groupId") Long id, @Path("userId") Long userId);
}
