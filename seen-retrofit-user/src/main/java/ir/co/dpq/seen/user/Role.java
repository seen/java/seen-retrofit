package ir.co.dpq.seen.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;

import ir.co.dpq.seen.Model;

/**
 * Role data model
 * 
 * @author maso <mostafa.barmshory@dpq.co.ir>
 *
 */
public class Role extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "version", defaultValue = "0", required = false, access = Access.WRITE_ONLY)
	@JsonPropertyDescription("A version")
	private Long version;

	@JsonProperty(value = "name", defaultValue = "name", required = false, access = Access.READ_WRITE)
	@JsonPropertyDescription("Name")
	private String name;

	@JsonProperty(value = "description", defaultValue = "description", required = false, access = Access.READ_WRITE)
	@JsonPropertyDescription("A description")
	private String description;

	@JsonProperty(value = "code_name", defaultValue = "permission", required = true, access = Access.READ_WRITE)
	@JsonPropertyDescription("Code name")
	private String codeName;

	@JsonProperty(value = "application", defaultValue = "core", required = false, access = Access.READ_WRITE)
	@JsonPropertyDescription("Application id")
	private String application;

	/**
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the codeName
	 */
	public String getCodeName() {
		return codeName;
	}

	/**
	 * @param codeName
	 *            the codeName to set
	 */
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	/**
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}

	/**
	 * @param application
	 *            the application to set
	 */
	public void setApplication(String application) {
		this.application = application;
	}

}
