package ir.co.dpq.seen.cms;

import com.fasterxml.jackson.annotation.JsonProperty;

import ir.co.dpq.seen.Model;

public class Content extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("title")
	private String title;

	@JsonProperty("description")
	private String description;

	@JsonProperty("mime_type")
	private String mimeType;

	@JsonProperty("file_name")
	private String fileName;

	@JsonProperty("file_size")
	private Long fileSize;

	@JsonProperty("download")
	private Long downloads;

	@JsonProperty("submitter")
	private Long submitter;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType
	 *            the mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the fileSize
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize
	 *            the fileSize to set
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the downloads
	 */
	public Long getDownloads() {
		return downloads;
	}

	/**
	 * @param downloads
	 *            the downloads to set
	 */
	public void setDownloads(Long downloads) {
		this.downloads = downloads;
	}

	/**
	 * @return the submitter
	 */
	public Long getSubmitter() {
		return submitter;
	}

	/**
	 * @param submitter
	 *            the submitter to set
	 */
	public void setSubmitter(Long submitter) {
		this.submitter = submitter;
	}

}
