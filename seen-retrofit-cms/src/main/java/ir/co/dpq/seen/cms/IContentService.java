package ir.co.dpq.seen.cms;

import java.util.Map;

import ir.co.dpq.seen.PaginatedList;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface IContentService {

	@GET("cms/find")
	Call<PaginatedList<Content>> list(@QueryMap Map<String, Object> queryParams);

	@FormUrlEncoded
	@POST("cms/new")
	Call<Content> create(@FieldMap Map<String, Object> params);

	@GET("cms/{contentId}")
	Call<Content> get(@Path("contentId") Long id);

	@FormUrlEncoded
	@POST("cms/{contentId}")
	Call<Content> update(@Path("contentId") Long id, @FieldMap Map<String, Object> params);

	@DELETE("cms/{contentId}")
	Call<Content> delete(@Path("contentId") Long id);

	@Multipart
	@POST("cms/{contentId}/download")
	Call<Content> upload(@Path("contentId") Long id, @Part("file") MultipartBody.Part file);

	@POST("cms/{contentId}/download")
	Call<ResponseBody> download(@Path("contentId") Long id);

}
