package ir.co.dpq.seen.monitor;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IPropertyService {

	@GET("monitor/{monitorId}/property/{propertyId}")
	Call<Property> get(@Path("monitorId") String monitor, @Path("propertyId") String property);
}
