/**
 * 
 */
package ir.co.dpq.seen.monitor;

import com.fasterxml.jackson.annotation.JsonProperty;

import ir.co.dpq.seen.Model;

/**
 * @author maso
 *
 */
public class Monitor extends Model {

	@JsonProperty("title")
	private String title;

	@JsonProperty("description")
	private String description;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
