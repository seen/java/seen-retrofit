package ir.co.dpq.seen.monitor;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import ir.co.dpq.seen.Util;

public class Property {

	@JsonProperty("type")
	private String type;

	@JsonProperty("value")
	private Object value;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * Convert to map
	 * 
	 * @see Util#convertValue(Object)
	 * @return
	 */
	public Map<String, Object> requestMap() {
		return Util.convertValue(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return Util.toString(this);
	}
}
