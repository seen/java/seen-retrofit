# Seen Retrofit client


## Add to your project

To get data from seen repository you can use the repository URLs. After that you can configure your repository in /path/to/project/pom.xml file:


	<project>
	    ...
	    <repositories>
	        <repository>
	            <id>Seen-Maven-Repo</id>
	            <url>https://mymavenrepo.com/repo/JayeaEMLm2LFsHP53lnX/</url>
	        </repository>
	    </repositories>
	    ...
	</project>
	
Addin seen-bom in your project dependency management:

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>ir.co.dpq.seen</groupId>
				<artifactId>seen-retrofit-bom</artifactId>
				<version>1.0.2</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

Finally add a module that you are looking for:

	<dependencies>
		<dependency>
			<groupId>ir.co.dpq.seen</groupId>
			<artifactId>seen-retrofit-user</artifactId>
		</dependency>
	</dependencies>


