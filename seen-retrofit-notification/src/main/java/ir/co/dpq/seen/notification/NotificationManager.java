package ir.co.dpq.seen.notification;

import java.util.Map;

import ir.co.dpq.seen.PaginatedList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Access layer of system notification
 * 
 * @author maso <mostafa.barmshory@dpq.co.ir>
 *
 */
public interface NotificationManager {

	@GET("message/find")
	Call<PaginatedList<Message>> list(@QueryMap Map<String, Object> queryParam);

	@FormUrlEncoded
	@POST("message/new")
	Callback<Message> create(@FieldMap Map<String, Object> params);

	@GET("message/{messageId}")
	Callback<Message> get(@Path("messageId") Long id);

	@FormUrlEncoded
	@POST("message/{messageId}")
	Callback<Message> update(@Path("messageId") Long id, @FieldMap Map<String, Object> params);

	@DELETE("message/{messageId}")
	Callback<Message> delete(@Path("messageId") Long id);
}
