package ir.co.dpq.seen.notification;

import com.fasterxml.jackson.annotation.JsonProperty;

import ir.co.dpq.seen.Model;

/**
 * ساختارهای داده برای پیام‌های سیستم را ایجاد می کند.
 * 
 * @author maso <mostafa.barmshory@dpq.co.ir>
 *
 */
public class Message extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("user")
	private Long user;
	
	@JsonProperty("message")
	private String message;


	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
